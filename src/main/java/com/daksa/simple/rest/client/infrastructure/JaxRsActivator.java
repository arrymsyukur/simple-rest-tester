package com.daksa.simple.rest.client.infrastructure;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@ApplicationPath("rest")
public class JaxRsActivator extends Application {
	
}
