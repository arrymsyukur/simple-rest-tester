package com.daksa.simple.rest.client.infrastructure;

import com.daksa.simple.rest.client.application.Config;
import java.io.IOException;
import java.io.InputStream;
import javax.naming.AuthenticationException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.HmacUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Zulkhair Abdullah Daim
 */
@WebFilter(filterName = "RestFilter", urlPatterns = {"/rest/*"})
public class RestFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(RestFilter.class);
    private final static String AUTH_MODE_DA01 = "DA01";
    private final static String ALLOWED_HEADERS = "origin, "
            + "content-type,"
            + " accept, "
            + "Authorization,"
            + "date,"
            + "name,"
            + "connection,"
            + "content-length,"
            + "rc,"
            + "x-powered-by,"
            + "server";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String resource = getResource(httpRequest);

        //Handle CORS
        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Origin", null);
        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Methods", "GET, OPTIONS, HEAD, PUT, POST");
        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Headers", ALLOWED_HEADERS);

        // For HTTP OPTIONS verb/method reply with ACCEPTED status code -- per CORS handshake
        if (httpRequest.getMethod().equals("OPTIONS")) {
            httpResponse.setStatus(HttpServletResponse.SC_ACCEPTED);
            return;
        }
        String method = httpRequest.getMethod();
        LOG.info("doFilter {} {}", method, resource);
        try {
            String authorization = httpRequest.getHeader("Authorization");
            String signature = null;
            if (authorization != null) {
                String[] authPart = authorization.split("\\s");
                if (authPart.length < 2) {
                    throw new AuthenticationException("Authorization header field is not valid");
                }
                String[] authPart2 = authPart[1].split(":");
                String authMode = authPart[0];
                if (!authMode.equals(AUTH_MODE_DA01)) {
                    throw new AuthenticationException("Unsupported authentication type");
                }
                if (authPart2 == null || authPart2.length < 2 || !authMode.equals(AUTH_MODE_DA01)) {
                    throw new AuthenticationException("Signature format is not valid");
                }
                signature = authPart2[1];
            }
            Config password = new Config("Password", "1234");
            String generatedSignature = generateSignature(httpRequest, resource, password.getName());
//            if (!generatedSignature.equals(signature)) {
//                throw new AuthenticationException("Signature is not match");
//            }
            chain.doFilter(request, response);
        } catch (AuthenticationException e) {
            LOG.error(e.getMessage(), e);
            httpResponse.addHeader("responseMessage", e.getMessage());
            httpResponse.setStatus(401);
        }
    }

    @Override
    public void destroy() {
    }

    private String getResource(HttpServletRequest servletRequest) {
        if (servletRequest.getContextPath() != null && !servletRequest.getContextPath().equals("/")) {
            return servletRequest.getRequestURI().substring(servletRequest.getContextPath().length());
        } else {
            return servletRequest.getRequestURI();
        }
    }

    private String generateSignature(HttpServletRequest httpRequest, String canonicalPath, String password) throws IOException {
        StringBuilder signatureBuilder = new StringBuilder();
        signatureBuilder.append(httpRequest.getMethod()).append("\n");
        if (httpRequest.getMethod().equalsIgnoreCase("POST") || httpRequest.getMethod().equalsIgnoreCase("PUT")) {
            if (httpRequest.getContentLength() > 0) {
                signatureBuilder.append(contentMD5(httpRequest)).append("\n");
            }
            if (httpRequest.getContentType() != null) {
                signatureBuilder.append(httpRequest.getContentType()).append("\n");
            }
        }
        signatureBuilder.append(httpRequest.getHeader("Date")).append("\n");
        signatureBuilder.append(canonicalPath);
        String digest = signatureBuilder.toString();
        String signature = Base64.encodeBase64String(HmacUtils.hmacSha1(password, digest));
        return signature;
    }

    private String contentMD5(HttpServletRequest httpRequest) throws IOException {
        try (InputStream input = httpRequest.getInputStream()) {
            String contentHashed = DigestUtils.md5Hex(input);
            return contentHashed;
        }
    }

}
