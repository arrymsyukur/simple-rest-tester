package com.daksa.simple.rest.client.application;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("apps")
public class ApplicationController implements Serializable {

    private static final long serialVersionUID = 1L;

    @GET
    public ApplicationModel getData(@QueryParam("id") String id) throws Exception {
        ApplicationModel model = new ApplicationModel("123", "Arry", new BigDecimal(500000));
        if (id != null) {
            if (!id.equals(model.getId())) {
                throw new Exception("ID Not Found");
            }
        }
        return model;

    }

    @POST
    public ApplicationModel createData(ApplicationModel model) {
        ApplicationModel applicationModel = new ApplicationModel();
        applicationModel.setId(model.getId());
        applicationModel.setName(model.getName());
        applicationModel.setBalance(model.getBalance());
        applicationModel.setStatus(Boolean.TRUE);

        return applicationModel;
    }

    @GET
    @Path("header")
    public ApplicationModel getDataUsingHeader(@HeaderParam("name") String name) throws Exception {
        ApplicationModel model = new ApplicationModel("123", "Arry", new BigDecimal(500000));
        if (name != null && !name.equals(model.getName())) {
            throw new Exception("Data Not Found");
        } else {
            return model;
        }

    }

}
